* Dotnet Core Introduction Training Notes
  * `DotnetCore_Training_Notes.pdf`
* Achieve Single Repsosibility in your code base
* SOC: Sepration of Concern
* Follow TDD Approach and start writing test case always(Unit Level)
* Use Trello Board or  Azure Boards to List out tasks and think like Agile
  for your daywise code practice
* Follow Layerd Approach for your Code Base
  * Controller------>Service------>Repository--------Context---->SQL
     * Exceptions 
     * ServiceFilter
     * Logging concepts to MVC and Web APIs
* Add XUnit level test cases to your projects
  (Conolse,MVC,WebAPIs)
*  Start Dockerizing you Web APIs for 
   details Check `DockerAndDockerComposeExplanation` Folder
* Docker:
     * SQL Server Image: 
      docker pull mcr.microsoft.com/mssql/server:2019-latest
      docker pull mcr.microsoft.com/mssql/server:2017-latest

    * .Net SDK Image
     docker pull mcr.microsoft.com/dotnet/sdk:5.0
     docker pull mcr.microsoft.com/dotnet/sdk:6.0


    * ASP.Net core Run time Image:
      docker pull mcr.microsoft.com/dotnet/aspnet:5.0

      docker pull mcr.microsoft.com/dotnet/aspnet:6.0


    # How to Create Image
    * docker build -t imagename:tagname
      `Note:` use imagename as small lettter
    
    # How to Run Docker image
    * docker run -p 8081:80 imagename
      `ImageName: weatherforecast:v1`
    * docker build -t weatherforecast:v1 .
    * docker run -p 5090:80 weatherforecast:v1

    # Docker Compose Command
      docker-compose up

   * How to Use Docker wiht ACR(Azure Container Registry)
     * docker login 5pcr.azurecr.io
     * username:5pcr
     * password:....................

  * docker build -t 5pcr.azurecr.io/wapi:v2 .