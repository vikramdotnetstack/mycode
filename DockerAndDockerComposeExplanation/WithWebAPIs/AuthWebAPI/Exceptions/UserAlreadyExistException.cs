﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthWebAPI.Exceptions
{
    public class UserAlreadyExistException:ApplicationException
    {
        public UserAlreadyExistException()
        {

        }
        public UserAlreadyExistException(string msg):base(msg)
        {

        }
    }
}
