﻿using Microsoft.EntityFrameworkCore;
using UserApp.APICore.Models;

namespace UserApp.APICore.Context
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
        {
            //Database
            //Database.EnsureCreated();
        }


        //Table Creation
        public DbSet<User> Users { get; set; }
    }
}
