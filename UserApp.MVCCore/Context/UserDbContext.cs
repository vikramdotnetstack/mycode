﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UserApp.MVCCore.Models;

namespace UserApp.MVCCore.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext>options):base(options)
        {
            //Database
            Database.EnsureCreated();
        }

        //Table Creation
        public DbSet<User> Users { get; set; }
    }
}
