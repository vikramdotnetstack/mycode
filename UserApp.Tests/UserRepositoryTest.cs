﻿using System;
using UserApp.Models;
using UserApp.Repository;
using Xunit;


namespace UserApp.Tests
{
    public class UserRepositoryTest
    {
        UserRepository userRepository;
        public UserRepositoryTest()
        {
            userRepository = new UserRepository();
        }
        [Fact]
        public void TestToCheckAddUserReturnSuccess()
        {
            // Arrange Initalization
            int expectedResult = 1;
            User user = new User() { Id = 1, Name = "User1" };
            //Act
            int actualResult = userRepository.AddUser(user);

            //Assert
            Assert.Equal(expectedResult, actualResult);
            
        }
        [Fact]
        public void TestToCheckGetAllUsersReturnSuccess()
        {
            //
            string expectedUserName = "User1";

            //Act
            string actualUserNameFromGetAllUsers = userRepository.GetAllUsers()[0].Name;

            Assert.Equal(expectedUserName, actualUserNameFromGetAllUsers);

        }
    }
}
