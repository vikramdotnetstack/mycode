﻿using AuthWebAPI.Models;

namespace AuthWebAPI.Services
{
    public interface IUserService
    {
        User RegisterUser(User user);
        User Login(string userName, string password);
        User FindUserById(string userName);
    }
}
