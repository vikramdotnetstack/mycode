﻿using AuthWebAPI.Exceptions;
using AuthWebAPI.Models;
using AuthWebAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthWebAPI.Services
{
    public class UserService : IUserService
    {
        IUserRepository userRepository;
        public UserService(IUserRepository _userRepository)
        {
            userRepository = _userRepository;
        }
        public User FindUserById(string userName)
        {
            return userRepository.FindUserById(userName);
        }

        public User Login(string userName, string password)
        {
            var userLoginDetails=userRepository.Login(userName, password);
            if (userLoginDetails != null)
            {
                return userLoginDetails;
            }
            else
            {
                throw new UserNotFoundException($"User {userName} not Found!!");
            }
        }

        public User RegisterUser(User user)
        {
            var userDetails=userRepository.FindUserById(user.Id);
            if (userDetails == null)
            {
                userRepository.RegisterUser(user);
                return user;
            }
            else
            {
                throw new UserAlreadyExistException($"User:{user.Id} Exist!!");
            }
        }
    }
}
