﻿using System.Collections.Generic;
using UserApp.Exceptions;
using UserApp.Models;

namespace UserApp.Repository
{
    public class UserRepository : IUserRepository
    {
        List<User> _users;
        public UserRepository()
        {
            _users = new List<User>();
        }
        public int AddUser(User user)
        {
            var userExist = GetUSerByName(user.Name);
            if (userExist == null)
            {
                _users.Add(user);
                return 1;
            }
            else
            {
                throw new UserNameExistException("User Exist!!");
            }


        }

        public List<User> GetAllUsers()
        {
            return _users;
        }

        public User GetUSerByName(string userName)
        {
            return _users.Find(u => u.Name == userName);
        }
    }
}
