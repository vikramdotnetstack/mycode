﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace UserApp.APICore.Models
{
    public class User
    {
        #region Properties  
        [JsonIgnore]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool IsRegular { get; set; } = true;
        
        #endregion

        public override string ToString()
        {
            return $"{Id}\t{Name}\t{IsRegular}";
        }
    }
}
