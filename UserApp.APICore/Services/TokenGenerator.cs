﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System;
using System.Security.Claims;
using Newtonsoft.Json;

namespace UserApp.APICore.Services
{
    public class TokenGenerator : ITokenGenerator
    {
        public string GenerateToken(int id,string name)
        {
            var userClaims = new Claim[]
            {
            new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString()),
            new Claim(JwtRegisteredClaimNames.UniqueName,name)
            };

            string secretKey = "lskhjfkhaskldhfkashklfhaslkd";
            var secretKeyInBytes = Encoding.UTF8.GetBytes(secretKey);
            var symmetricSecurityKey = new SymmetricSecurityKey(secretKeyInBytes);
            var userSigningCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            var userToken = new JwtSecurityToken(
                issuer: "UserAPI",
                audience: "ProductAPI",
                signingCredentials: userSigningCredentials,
                expires: DateTime.UtcNow.AddMinutes(5),
                claims: userClaims
                
                );

            var userJwtToken = new { token = new JwtSecurityTokenHandler().WriteToken(userToken) };
            return JsonConvert.SerializeObject(userJwtToken);
            //return jwtSecurityTokenHandler;
        }
    }
}
