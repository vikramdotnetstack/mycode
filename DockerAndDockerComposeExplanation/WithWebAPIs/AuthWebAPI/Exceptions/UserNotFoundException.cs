﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthWebAPI.Exceptions
{
    public class UserNotFoundException:ApplicationException
    {
        public UserNotFoundException()
        {

        }
        public UserNotFoundException(string msg):base(msg)
        {

        }
    }
}
