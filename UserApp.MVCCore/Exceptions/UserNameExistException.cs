﻿using System;

namespace UserApp.MVCCore.Exceptions
{
    class UserNameExistException : ApplicationException
    {
        public UserNameExistException()
        {

        }
        public UserNameExistException(string message) : base(message)
        {

        }

    }
}
