﻿using AuthWebAPI.Models;

namespace AuthWebAPI.Repository
{
    public interface IUserRepository
    {
        User RegisterUser(User user);
        User Login(string userName, string password);
        User FindUserById(string userName);
    }
}
