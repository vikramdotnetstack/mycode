﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserApp.MVCCore.Models;
using UserApp.MVCCore.Services;

namespace UserApp.MVCCore.Controllers
{
    public class UserController : Controller
    {
        //new X
        readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        public IActionResult Index()
        {
            List<User> users = _userService.GetAllUsers();
            return View(users);
        }

        [HttpGet]
        public ActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddUser(User user)
        {
            int userAddStatus=_userService.AddUser(user);
            return RedirectToAction("Index");
        }

    }
}
