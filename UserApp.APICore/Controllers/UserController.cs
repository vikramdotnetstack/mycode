﻿using Microsoft.AspNetCore.Mvc;
using UserApp.APICore.Models;
using UserApp.APICore.Services;
using UserApp.APICore.Exceptions;
using System;
using UserApp.APICore.AspectLogic;

namespace UserApp.APICore.Controllers
{
    [ServiceFilter(typeof(CustomLogger))]
    [ExceptionHandler]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService _userService;
        readonly ITokenGenerator _tokenGenerator;
        public UserController(IUserService userService,ITokenGenerator tokenGenerator)
        {
            _userService = userService;
            _tokenGenerator = tokenGenerator;
        }
        [HttpGet]
        [Route("getAllUsers")]
        public ActionResult GetAllUsers()
        {
            
            return Ok(_userService.GetAllUsers());
        }

        [HttpPost]
        [Route("register")]
        public ActionResult RegisterUser([FromBody] User user)
        {
           return Ok(_userService.AddUser(user));
        }
        [HttpPost]
        [Route("login")]
        public ActionResult Login([FromBody] UserLoginInfo userLoginInfo)
        {
            User userLoginInfoDetails =_userService.Login(userLoginInfo);
            //Generate Token
            string userToken=_tokenGenerator.GenerateToken(userLoginInfoDetails.Id,userLoginInfoDetails.Name);
            return Ok(userToken);

        }
    }
}
