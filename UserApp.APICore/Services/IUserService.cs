﻿using System.Collections.Generic;
using UserApp.APICore.Models;

namespace UserApp.APICore.Services
{
    public interface IUserService
    {
        List<User> GetAllUsers();
        int AddUser(User user);
        User Login(UserLoginInfo userLoginInfo);
    }
}
