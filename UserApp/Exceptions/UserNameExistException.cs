﻿using System;

namespace UserApp.Exceptions
{
    class UserNameExistException : ApplicationException
    {
        public UserNameExistException()
        {

        }
        public UserNameExistException(string message) : base(message)
        {

        }

    }
}
