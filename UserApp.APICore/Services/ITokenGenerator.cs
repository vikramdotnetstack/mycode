﻿namespace UserApp.APICore.Services
{
    public interface ITokenGenerator
    {
        string GenerateToken(int id,string name);
    }
}
