﻿using System.Collections.Generic;
using UserApp.APICore.Exceptions;
using UserApp.APICore.Models;
using UserApp.APICore.Repository;

namespace UserApp.APICore.Services
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public int AddUser(User user)
        {
            var userExist = _userRepository.GetUSerByName(user.Name);
            if (userExist == null)
            {
                return _userRepository.AddUser(user);

            }
            else
            {
                throw new UserNameExistException($"{user.Name} Exist!!");
            }
        }



        public List<User> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

        public User Login(UserLoginInfo userLoginInfo)
        {
           return _userRepository.Login(userLoginInfo);
        }
    }
}
