﻿namespace UserApp.Models
{
    public class User
    {
        #region Properties  
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsRegular { get; set; } = true;
        #endregion

        public override string ToString()
        {
            return $"{Id}\t{Name}\t{IsRegular}";
        }
    }
}
