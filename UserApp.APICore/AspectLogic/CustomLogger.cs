﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.AspNetCore.Hosting;
using System.IO;
namespace UserApp.APICore.AspectLogic
{
    public class CustomLogger:ActionFilterAttribute
    {
        readonly string fileName = null;
        DateTime startTime;
        DateTime endTime;
        TimeSpan totalTime;
        string rootPath = null;
        string filePath = null;
        
        public CustomLogger(IWebHostEnvironment environment)
        {
            rootPath = environment.ContentRootPath;
            filePath = rootPath + @$"/Logfile/CustomLogFile.txt";

        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            startTime = DateTime.Now;
            ControllerBase controllerBase = (ControllerBase)context.Controller;
            var contextController = controllerBase.ControllerContext.ActionDescriptor;
            string contorllerName = contextController.ControllerName;
            string actionName = contextController.ActionName;
            using(StreamWriter writer= new StreamWriter(filePath,true))
            {
                writer.Write($"StartTime::{startTime}\tActionName::{actionName}\tControllerName::{contorllerName}");
            }

        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            endTime = DateTime.Now;
            totalTime = endTime - startTime;
            using (StreamWriter writer = new StreamWriter(filePath,true))
            {
               writer.WriteLine($"\nTotalTime::{totalTime}");
            }
            base.OnActionExecuted(context);
        }

    }
}
