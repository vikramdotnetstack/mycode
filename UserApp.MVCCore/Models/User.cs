﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace UserApp.MVCCore.Models
{
    public class User
    {
        #region Properties  
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsRegular { get; set; } = true;
        #endregion

        public override string ToString()
        {
            return $"{Id}\t{Name}\t{IsRegular}";
        }
    }
}
