﻿using System.Collections.Generic;
using System.Linq;
using UserApp.APICore.Context;
using UserApp.APICore.Models;

namespace UserApp.APICore.Repository
{
    public class UserRepository : IUserRepository
    {
        readonly UserDbContext _userDbContext;
        public UserRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }
        public int AddUser(User user)
        {
            #region id generation Code
            //var usersList = _userDbContext.Users.AsQueryable();
            //if (usersList.Count() == 0)
            //{
            //    user.Id = 1;
            //    _userDbContext.Users.Add(user);
            //    _userDbContext.SaveChanges();
            //}
            //else
            //{
            //    user.Id = usersList.Max(u => u.Id) + 1;
            //}

            //return _userDbContext.SaveChanges();
            #endregion

            _userDbContext.Users.Add(user);
            return _userDbContext.SaveChanges();


        }

        public List<User> GetAllUsers()
        {
            return _userDbContext.Users.ToList();
        }

        public User GetUSerByName(string userName)
        {
            return _userDbContext.Users.Where(u => u.Name == userName).FirstOrDefault();
        }

        public User Login(UserLoginInfo userLoginInfo)
        {
            return _userDbContext.Users.Where(u => u.Name == userLoginInfo.Name && u.Password == userLoginInfo.Password).FirstOrDefault();
        }
    }
}
