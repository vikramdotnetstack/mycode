﻿using AuthWebAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace AuthWebAPI.Repository
{
    public class UserContext:DbContext
    {
        //Add DbContext
        public UserContext(DbContextOptions<UserContext>options):base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<User> users { get; set; }
    }
}
