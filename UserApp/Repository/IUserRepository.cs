﻿using System.Collections.Generic;
using UserApp.Models;

namespace UserApp.Repository
{
    public interface IUserRepository
    {
        public int AddUser(User user);
        public List<User> GetAllUsers();
        public User GetUSerByName(string userName);
    }
}
