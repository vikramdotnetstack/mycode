﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthWebAPI.Services
{
    public interface ITokenGenerator
    {
        string JWTToken(string userId);
    }
}
