﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserApp.MVCCore.Models;
using UserApp.MVCCore.Repository;
using UserApp.MVCCore.Exceptions;

namespace UserApp.MVCCore.Services
{
    public class UserService:IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public int AddUser(User user)
        {
            var userExist=_userRepository.GetUSerByName(user.Name);
            if (userExist == null)
            {
                return _userRepository.AddUser(user);

            }
            else
            {
                throw new UserNameExistException($"{user.Name} Exist!!");
            }
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

    }
}
