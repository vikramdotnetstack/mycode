﻿using System.Collections.Generic;
using UserApp.MVCCore.Models;

namespace UserApp.MVCCore.Repository
{
    public interface IUserRepository
    {
        public int AddUser(User user);
        public List<User> GetAllUsers();
        public User GetUSerByName(string userName);
    }
}
