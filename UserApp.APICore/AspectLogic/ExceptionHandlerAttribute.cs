﻿using Microsoft.AspNetCore.Mvc.Filters;
using UserApp.APICore.Exceptions;
using Microsoft.AspNetCore.Mvc;
namespace UserApp.APICore.AspectLogic
{
    public class ExceptionHandlerAttribute:ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(UserNameExistException))
            {
                context.Result=new NotFoundObjectResult(context.Exception.Message);
            }
            else
            {
                context.Result = new StatusCodeResult(500);
            }
        }
    }
}
