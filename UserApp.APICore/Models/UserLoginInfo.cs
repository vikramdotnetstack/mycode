﻿namespace UserApp.APICore.Models
{
    public class UserLoginInfo
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
