﻿using System;

namespace UserApp.APICore.Exceptions
{
    class UserNameExistException : ApplicationException
    {
        public UserNameExistException()
        {

        }
        public UserNameExistException(string message) : base(message)
        {

        }

    }
}
