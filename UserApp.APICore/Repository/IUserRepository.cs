﻿using System.Collections.Generic;
using UserApp.APICore.Models;

namespace UserApp.APICore.Repository
{
    public interface IUserRepository
    {
        public int AddUser(User user);
        public List<User> GetAllUsers();
        public User GetUSerByName(string userName);
        User Login(UserLoginInfo userLoginInfo);
    }
}
