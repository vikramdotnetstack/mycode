﻿using AuthWebAPI.Models;
using System.Linq;

namespace AuthWebAPI.Repository
{
    public class UserRepository : IUserRepository
    {
        UserContext userContext;
        public UserRepository(UserContext _userContext)
        {
            userContext = _userContext;
        }
        public User FindUserById(string userName)
        {
            return userContext.users.Find(userName);

        }

        public User Login(string userName, string password)
        {
            return userContext.users.FirstOrDefault(u => u.Id == userName && u.Password == password);
        }

        public User RegisterUser(User user)
        {
            userContext.users.Add(user);
            userContext.SaveChanges();
            return user;
        }
    }
}
