﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserApp.MVCCore.Models;

namespace UserApp.MVCCore.Services
{
    public interface IUserService
    {
        List<User> GetAllUsers();
        int AddUser(User user);
    }
}
