﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace AuthWebAPI.Services
{
    public class TokenGenerator : ITokenGenerator
    {
        public string JWTToken(string userId)
        {
            var userClaims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName,userId),
                new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString())
            };

            var userKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("USTAuthenticationAPIKEYforSecurity"));
            var userCredentials = new SigningCredentials(userKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "AuthWebAPI",
                audience: "KeepNoteAPI",
                expires: DateTime.UtcNow.AddMinutes(10),
                signingCredentials: userCredentials,
                claims: userClaims
                );
            var res=new{ token=new JwtSecurityTokenHandler().WriteToken(token) };
            var jsonObj=JsonConvert.SerializeObject(res);
            return jsonObj;
        }
    }
}
