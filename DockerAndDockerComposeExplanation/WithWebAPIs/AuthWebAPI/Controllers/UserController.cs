﻿using System;
using AuthWebAPI.Exceptions;
using AuthWebAPI.Models;
using AuthWebAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace AuthWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        IUserService userService;
        ITokenGenerator tokenGenerator;
        public UserController(IUserService _userService,ITokenGenerator _tokenGenerator)
        {
            userService = _userService;
            tokenGenerator = _tokenGenerator;
        }

        [HttpPost]
        [Route("register")]
        public ActionResult Register([FromBody] User user)
        {
            try
            {
                userService.RegisterUser(user);
                return StatusCode(201, "User Created Successfully!!");
            }
            catch (UserAlreadyExistException uaex)
            {
                return StatusCode(409, $"{uaex.Message}--Conflict!!");
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal Server Error");
            }

        }

        [HttpPost]
        [Route("login")]
        public ActionResult Login(string userName, string password)
        {
            try
            {
                var userDetails = userService.Login(userName, password);
                var token = tokenGenerator.JWTToken(userDetails.Id);
                return Ok(token);

            }
            catch (UserNotFoundException unfe)
            {
                return NotFound(unfe.Message);
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal Server Error");
            }
        }

    }
}
